function calculateIncidentCosts(incident){
	//this is a seconds to interval mapping table used in the calculation of the failure time
	var timeMap = new Object();
	timeMap.daily = 86499;
	timeMap.weekly = 604800;
	timeMap.weekly2 = timeMap.weekly * 2;
	timeMap.monthly = 2630000;
	timeMap.monthly2 = timeMap.monthly * 2;
	timeMap.quarterly = timeMap.yearly / 4;
	timeMap.semiannual = timeMap.yearly / 2;
	timeMap.yearly = 31560000;
	
	//geting the incident
	var inc = new GlideRecord('incident');
	inc.get(incident.sys_id);
	
	//setting total failure cost variable to scope for use outside of GlideRecord Query
	var totalFailureCost = 0;
	
	//get all outages associated with this incident		
	var outage = new GlideRecord('cmdb_ci_outage');
	outage.addQuery('task_number', inc.sys_id);
	outage.query(); 
	while (outage.next()) {
		//calculate the total time of the outage
		var outageTime = gs.dateDiff(outage.begin, outage.end, true);
		var outageFailureCost = 0;
		
		//find all rate cards associated with the outage CI
		var rtCardm2m = new GlideRecord('fm_ci_rate_card_cmdb_ci_m2m');
		rtCardm2m.query('cmdb_ci', outage.cmdb_ci.sys_id);
		while (rtCardm2m.next()) {
			//get rate card cost record associated with outage rate cards
			var rtCardCost = new GlideRecord('fm_ci_rate_card_cost');
			rtCardCost.addQuery('rate_card', rtCardm2m.rate_card.sys_id);
			rtCardCost.query('u_outage_related', true); 
			rtCardCost.query(); 
			//calculate cost of outage
			while (rtCardCost.next()) {
				outageFailureCost = outageTime * (rtCardCost.base_cost / timeMap[rtCardCost.interval]);
				totalFailureCost = totalFailureCost + outageFailureCost;
			}
		}
	}
	
	//Calculating the total cost of labor based on expense lines that include "Time Cards"
	var totalLaborCost = 0;
	var expenseLine = new GlideRecord('fm_expense_line');
	expenseLine.addQuery('source_id', inc.sys_id);
	expenseLine.addQuery('short_description', 'CONTAINS', 'Time Card');
	expenseLine.query(); 
	while (expenseLine.next()) {
		totalLaborCost = totalLaborCost + parseFloat(expenseLine.amount);
	}

	//set the fields on the incident form
	inc.u_labor_cost = totalLaborCost;
	inc.u_failure_cost = totalFailureCost;
	inc.u_total_cost = parseFloat(inc.u_labor_cost) + parseFloat(inc.u_failure_cost);	
	inc.update();	
}