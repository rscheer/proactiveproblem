function checkCategories() {
	var ids = GlideList2.get('u_proactive_problem').getChecked();
	if (ids.length > 0) {
		var validateCheckedIncidents = new GlideAjax('createProactiveProblem');
		validateCheckedIncidents.addParam('sysparm_name', 'validateUniqueCategories');
		validateCheckedIncidents.addParam('sysparm_proPrbs', ids);
		validateCheckedIncidents.getXML(validateUniqueCategories);
	} else {
		var conf = confirm("If you want to create a problem from all records in this query, click \"Ok\"");
		if(conf){
			//make script include call and pass it the encoded query
			var validateCheckedIncidents = new GlideAjax('createProactiveProblem');
			validateCheckedIncidents.addParam('sysparm_name', 'validateUniqueCategories');
			validateCheckedIncidents.addParam('sysparm_listquery', g_list.getQuery());
			validateCheckedIncidents.getXML(validateUniqueCategories);		
		} 
	}
}

function validateUniqueCategories(response){
	var answer = response.responseXML.documentElement.getAttribute("answer");
	if (answer == 'false'){
		alert("You have to choose incidents within a single category of Knowledge");
		return;
	} else {
		createProblem(answer);
	}
}

function createProblem(ids){
	var ajax = new GlideAjax('createProactiveProblem');
	ajax.addParam('sysparm_name', 'createProactiveProblem');
	ajax.addParam('sysparm_proPrbs', ids);
	ajax.getXML(redirectURL);
}

function redirectURL(response) {
	var newSite = response.responseXML.documentElement.getAttribute("answer");
	var url = 'problem.do?sys_id=' + newSite;
	window.location = url;
}