generateDemoData();

function generateDemoData(){
	var demoTestType = "\nWF Disabled on: INC, KB, OUT, Timecard, expense line\ncalculateIncidents is NOT active in script, not processing timecard";
	var failureCosts = createfailureCostHash();


	var recordCount = 1000;

	var startTime = new Date();
	gs.log('>>>RSS Beginning Proative Problem Demo Data Generation at ' + startTime);


	var dataArray = [

	sapMissingInventory = new demoData("Cannot find food color red #11", "SAP missing juice concentrate levels at Hawaii warehouse", "Missing Inventory", "SAP Inventory Loader"),

	sapIncorrectInventory = new demoData("Showing 12k pineapples instead of actual 10k", "Report is Incorrect", "Incorrect Inventory", "SAP Inventory Loader"),

	sapOutdatedInventory = new demoData("May inventory not showing", "Shipment received yesterday is not in system", "Out of Date Inventory", "SAP Inventory Loader"),

	sapInventoryNotShowing = new demoData("Inventory disappeared in system", "Showing 0 bananas in Costa Rica site", "Inventory Not Showing", "SAP Inventory Loader"),

	userForgotPassword = new demoData("My password isn't working","Need to reset password", "Forgot password", "User Error"),

	employeeOnboarding = new demoData("Need to hire a new employee","Help with onboarding"
	, "How to start the onboarding process", "User Error"),

	newAccountRequest = new demoData("Need an email account", "Access to Workday", "How to request a new account", "User Error"),

	diskRunningSlow = new demoData("Computer running slow","Programs running slowly", "Disk running slow", "Hardware - Disk"),

	diskNotResponding = new demoData("Computer asking me to start in safe-mode","E: drive disappearing", "Disk not responding", "Hardware - Disk"),

	diskFailure = new demoData("\"Windows detected a hard disk problem\"","getting message 1720 – Imminent Hard Disk Failure", "Disk failure", "Hardware - Disk"),

	vpnIsDown = new demoData("Cannot connect to office network","Network fileshare not available", "VPN is down", "External"),

	externalNetworkNotResponding = new demoData("Internet is down!!", "Acme site is not working", "External network not responding", "External"),

	spamFilterNotWorking = new demoData("Recieved 20 spam emails last night", "Emails not sending", "Spam Filter Not Working", "External")

	];

	for (var i=0; i < recordCount; i++) {
		var obj = dataArray[Math.floor(Math.random()*dataArray.length)];
		obj.setRandomDates();

		var key = getKeyByValue(obj.cmdb_ci, failureCosts);

		var k = new GlideRecord('kb_knowledge');
		k.addQuery('short_description', obj.kb_article);
		k.query();
		while (k.next()){
			obj.kb_article = k;
		}

		var inc = new GlideRecord('incident');
		inc.initialize();
		inc.short_description = obj.short_description;
		inc.cmdb_ci = obj.cmdb_ci;
		inc.opened_at = obj.start_date;
		inc.close_code = 'Solved (Permanently)';
		inc.close_notes = "Closed using KB Article: " + obj.kb_article.number + '   ' + obj.kb_article.short_description;
		inc.state = 7;
		inc.setWorkflow(false);

		inc.closed_at = obj.end_date;
		inc.assignment_group.setDisplayValue('Service Desk');
		inc.assigned_to = '681b365ec0a80164000fb0b05854a0cd';
		var start_end_diff = gs.dateDiff(inc.opened_at, inc.closed_at, true);

		if (obj.cmdb_ci){
			inc.u_failure_cost = start_end_diff * failureCosts[key].costPerSecond;
		}
		inc.insert();


		var attachedKnowledge = new GlideRecord('m2m_kb_task');
		attachedKnowledge.initialize();
		attachedKnowledge.setWorkflow(false);
		attachedKnowledge.task =inc.sys_id;
		attachedKnowledge.kb_knowledge = obj.kb_article.sys_id;
		attachedKnowledge.insert();


		if (obj.category != 'User Error'){
			var outage = new GlideRecord('cmdb_ci_outage');
			outage.initialize();
			outage.setWorkflow(false);		
			outage.type = 'outage';
			outage.cmdb_ci = obj.cmdb_ci;
			outage.begin = obj.start_date;
			outage.end = obj.end_date;
			outage.task_number =inc.sys_id;
			outage.short_description = outage.cmdb_ci.getDisplayValue().toString() + " Outage";
			outage.insert();
			
			var outagem2m = new GlideRecord('task_outage');
			outagem2m.initialize();
			
			outagem2m.task = inc.sys_id;
			outagem2m.outage = outage.sys_id;
			outagem2m.setWorkflow(false);
			outagem2m.insert();
			
					
		}

		var timeCard = new GlideRecord('time_card');
		timeCard.initialize();
		timeCard.week_starts_on = '2013-04-10';
		timeCard.category = 'task_work';
		timeCard.task =inc.sys_id;
		timeCard.user = '681b365ec0a80164000fb0b05854a0cd';
		timeCard.tuesday = 10;
		timeCard.total = 10;
		timeCard.setWorkflow(false);	
		timeCard.state = 'Approved';
		timeCard.insert();

		var expenseLine = new GlideRecord('fm_expense_line');
		expenseLine.initialize();
		expenseLine.setWorkflow(false);
		expenseLine.date = gs.getDate();
		expenseLine.source_id =inc.sys_id;
		expenseLine.ci = obj.cmdb_ci;
		expenseLine.task =inc.sys_id;
		expenseLine.short_description = inc.number + ' Time Card (' + inc.assigned_to.getDisplayValue() + ' ' + timeCard.week_starts_on + ')';
		expenseLine.state = 'processed';
		expenseLine.process_date = gs.getDateTime();
		expenseLine.amount = timeCard.total * obj.laborCost;
		expenseLine.insert();


		inc.u_labor_cost = expenseLine.amount;
		inc.u_total_cost = parseFloat(expenseLine.amount) + parseFloat(inc.u_failure_cost);
		inc.update();


		//processTimecardCost(timeCard);

		//calculateIncidentCosts(inc);		

		//gs.print("Incident created: " + "KB Article: " + obj.kb_article.short_description);

		//gs.print("Incident created: " + "KB Article: " + obj.kb_article.short_description + "; Short Description: " + obj.short_description + "; Start Date: " + obj.start_date + "; End Date: " + obj.end_date);
	};

	var endTime = new Date();
	gs.log(">>>RSS Proactive Problem Demo Data Finished Loading at " + endTime);
	var time = Math.round((endTime - startTime)/1000);
	var minutes = Math.floor(time / 60);
	var seconds = time - minutes * 60;
	gs.setTimeout(function(){}, 3000);
	gs.log(">>>RSS Total Duration (minutes:seconds) : " + minutes + ":" + seconds + "\nNumber of Records: " + recordCount + "\nRecords generated per second: " + recordCount / time + demoTestType);


	function processTimecardCost(timecard) {
		var trp = new TaskRateProcessor();
		trp.processTaskTimecard(timecard);

		//update state to processed so we dont process again
		timecard.state = "Processed";
		timecard.update();
	}

	function demoData(short_description1, short_description2, kb_article, category){
		this.descriptions=[short_description1, short_description2];
		this.kb_article=kb_article;
		this.category=category;
		this.short_description = this.descriptions[Math.floor(Math.random()*this.descriptions.length)];
		this.cmdb_ci = '';
		this.laborCost = 75;
		switch(this.category){		
			case "Hardware - Disk":
			this.cmdb_ci = 'a9a2d111c611227601fb37542399caa8';
			break;

			case "SAP Inventory Loader":
			this.cmdb_ci = '26da329f0a0a0bb400f69d8159bc753d';
			this.laborCost = 200;
			break;

			case "External":
			this.cmdb_ci = '27d32778c0a8000b00db970eeaa60f16';
			break;

			case "User Error":
			gs.log("No CI in outage because of User Error");
			break;

			default:
			gs.log("No category found in object for CI association");
			break;
		}

		this.setRandomDates = function(){
			this.start_date = this.randomDate(new Date(2013, 3, 1), new Date(2013, 4, 1));

			var d = new Date(this.start_date);
			d.setHours(d.getHours() + 7);

			this.end_date = this.randomDate(this.start_date, d);

			this.start_date = this.convertJavascriptDatetoSncDate(this.start_date);
			this.end_date = this.convertJavascriptDatetoSncDate(this.end_date);		
		}
		this.randomDate= function(start, end){
			return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
		}
		this.convertJavascriptDatetoSncDate = function(date){
			var monthMap = new Object();
			monthMap.Jan = '01';
			monthMap.Feb = '02';
			monthMap.Mar = '03';
			monthMap.Apr = '04';
			monthMap.May = '05';
			monthMap.Jun = '06';
			monthMap.Jul = '07';
			monthMap.Aug = '08';
			monthMap.Sep = '09';
			monthMap.Oct = '10';
			monthMap.Nov = '11';
			monthMap.Dec = '12';

			var matchDate = date.toString();

			var match = matchDate.match(/^(\w+) (\w+) (\d+) (\d+) (\d+):(\d+):(\d+) /);
			var sncDate = match[4] + '-' + monthMap[match[2]] + '-' + match[3] + ' ' + match[5] + ':' + match[6] + ':' + match[7];

			return sncDate;
		}
	}

	function createfailureCostHash(){
		var failureCosts = new Object();

		failureCosts.sap = {sys_id : '26da329f0a0a0bb400f69d8159bc753d'};
		failureCosts.computer = {sys_id : 'a9a2d111c611227601fb37542399caa8'};
		failureCosts.email = {sys_id : '27d32778c0a8000b00db970eeaa60f16'};


		for (var i in failureCosts){
			var timeMap = new Object();
			timeMap.daily = 86499;
			timeMap.weekly = 604800;
			timeMap.weekly2 = timeMap.weekly * 2;
			timeMap.monthly = 2630000;
			timeMap.monthly2 = timeMap.monthly * 2;
			timeMap.quarterly = timeMap.yearly / 4;
			timeMap.semiannual = timeMap.yearly / 2;
			timeMap.yearly = 31560000;

			var rtCardm2m = new GlideRecord('fm_ci_rate_card_cmdb_ci_m2m');
			rtCardm2m.query('cmdb_ci', failureCosts[i].sys_id);
			while (rtCardm2m.next()) {
				//get rate card cost record associated with outage rate cards
				var rtCardCost = new GlideRecord('fm_ci_rate_card_cost');
				rtCardCost.addQuery('rate_card', rtCardm2m.rate_card.sys_id);
				rtCardCost.query('u_outage_related', true); 
				rtCardCost.query(); 
				//calculate cost of outage
				while (rtCardCost.next()) {
					failureCosts[i].costPerSecond = rtCardCost.base_cost / timeMap[rtCardCost.interval];
				}
			}
		}
		return failureCosts;
	}


	function getKeyByValue(value, obj){
		//gs.print('print value ' + value);
		for( var prop in obj ) {
			//gs.print('print prop ' + prop);
			for( var i in obj[prop] ){
				//gs.print('print i ' + i);
				if( obj[ prop ][i] === value )
				return prop;
			}
		}
	}
}