var createProactiveProblem = Class.create();
createProactiveProblem.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	createProactiveProblem: function(){
		var category;
		//get sys_id's from UI Action
		var sids = this.getParameter('sysparm_proPrbs');
		sids = sids.split(',');

		//create problem record
		var p = new GlideRecord('problem');
		//p.short_description = 'Problem Regarding: ';
		p.insert();

		//for each record selected, update the associated incident with the problem we just created
		for (var i=0; i < sids.length; i++){
			var proPrb = new GlideRecord('u_proactive_problem');
			proPrb.addQuery('inc_sys_id', sids[i]);
			proPrb.query();
			category = proPrb.knot_kb_knowledge.u_problem_category;
			while(proPrb.next()){
				inc = new GlideRecord('incident');
				inc.get(proPrb.knot_task.sys_id);
				inc.problem_id = p.sys_id;
				inc.update();
				category = proPrb.knot_kb_knowledge.u_problem_category.getTextAreaDisplayValue();
				gs.log('>>>RSS Problem Category: ' + proPrb.knot_kb_knowledge.u_problem_category.getTextAreaDisplayValue());
			}
		}

		p.short_description = "Problem Regarding: " + category;
		p.update();


		var problemId = p.sys_id;
		return problemId;
	},


	validateUniqueCategories: function(){
		var categories = [];
		var counter = [];


		var ids = this.getParameter('sysparm_proPrbs');
		ids = ids.split(',');



		for (var i=0; i < ids.length; i++){
			var proPrb = new GlideRecord('u_proactive_problem');
			proPrb.addQuery('inc_sys_id', ids[i]);
			proPrb.query();	
			while(proPrb.next()){			
				categories.push(proPrb.knot_kb_knowledge.u_problem_category);
				gs.log('>>>RSS validate: pushed ' + proPrb.knot_kb_knowledge.u_problem_category + ' into array');			
			}
		}


		counter = removeArrayDuplicates(categories);
		
		gs.log("Counters array has " + counter.length);


		if (counter.length > 1){
			gs.log('>>>RSS validate: returned false');
			return false;
		} else {
			gs.log('>>>RSS validate: returned true');
			return true;
		}
	
		function removeArrayDuplicates(a) {
			var r = new Array(); //Create a new array to be returned with unique values
			o:for(var i = 0, n = a.length; i < n; i++){
				for(var x = 0, y = r.length; x < y; x++){
					if(r[x]==a[i]){
						continue o;
					}
				}
				r[r.length] = a[i];
			}
			return r;
		}
	}
});